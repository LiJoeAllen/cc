package com.example.cc;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author ht
 */
public class Utils {
    @NotNull
    @Contract("_ -> new")
    public static InputStream get(String filePath) throws FileNotFoundException {
        return new FileInputStream(filePath);
    }
}
