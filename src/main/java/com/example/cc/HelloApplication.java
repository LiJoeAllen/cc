package com.example.cc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.*;

/**
 * @author ht
 */
public class HelloApplication extends Application {
    @Override
    public void start(@NotNull Stage stage) throws IOException {
        Application.setUserAgentStylesheet(null);
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
        /*
         * corePoolSize线程池的核心线程数
         * maximumPoolSize能容纳的最大线程数
         * keepAliveTime空闲线程存活时间
         * unit 存活的时间单位
         * workQueue 存放提交但未执行任务的队列
         * threadFactory 创建线程的工厂类
         * handler 等待队列满后的拒绝策略
         */
        ExecutorService threadPool = new ThreadPoolExecutor(2, 5, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(3), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
        threadPool.execute(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("分支线程--->" + i);
            }
        });


    }

    public static void main(String[] args) {
        launch();
    }
}